'use strict';

const ApplicationService = require('icontactpoints-core').ApplicationService;
const ApplicationAccessService = require('../applicationaccesses/application.access.service');

class AuthService {
  constructor() {
    this.applicationService = new ApplicationService();
    this.applicationAccessService = new ApplicationAccessService();
  }

  validate(options) {
    return Promise.resolve(options);
  }

  acquiesce(options) {
    if (options.type) {
      switch (options.type) {
        case 'code':
          return acquiesceCODE(options);

        case 'token':
          return ;
        
        case 'password':
          return ;
        
        default:
          return Promise.reject({ code: 400, message: 'Invalid type provided'}); 
      }
    } else {
      return Promise.reject({ code: 400, message: 'No type specified' });
    }

    function acquiesceCODE(options) {
      this.applicationService.getApplicationByClientID(options.clientID)
        .then(application => {
          return this.applicationAccessService.getByAppRefex(application.refex);
        })
        .then(applicationAccess => {
          return applicationAccess.getAuthorizeCode();
        })
    }
  }

  authorize(options) {
    

    function authorizeCODE(options) {
      this.applicationService.getApplicationByClientID(options.clientID)
        .then(application => {
          this.applicationAccessService.getByAppRefex
        })
    }

    function authorizeTOKEN(options) {

    }

    function authorizePASSWORD(options) {

    }
  }
}

module.exports = AuthService;