'use strict';

const DataContext = require('archdatacore').DataContext;
const ApplicationAccess = require('./application.access.model');

const collectionName = 'ApplicationAccesses';

class ApplicationAccessDataService {
  constructor() {
    this.dataContext = new DataContext();
  }

  createApplicationAccess(applicationAccess, callback) {
    const filter = { app_refex: applicationAccess.app_refex };

    if (callback) {
      this.dataContext.findOne(collectionName, filter)
      .then(result => {
        if (result.found > 0) {
          callback(null, { success: true, affected: 1, data: [result.data] });
        } else {
          this.dataContext.insertOne(collectionName, applicationAccess, null, callback);
        }
      })
      .catch(error => callback(error));
    } else {
      return this.dataContext.findOne(collectionName, filter)
      .then(result => {
        if (result.found > 0) {
          return Promise.resolve({ success: true, affected: 1, data: [result.data] });
        } else {
          return this.dataContext.insertOne(collectionName, applicationAccess);
        }
      })
    }
  }

  getApplicationAccessByAppRefex(app_refex, callback) {
    const filter = { app_refex };

    return this.dataContext.findOne(collectionName, filter, null, callback);
  }
}

module.exports = ApplicationAccessDataService;