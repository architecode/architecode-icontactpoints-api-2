'use strict';

const ApplicationAccess = require('./application.access.model');
const ApplicationAccessDataService = require('./application.access.dataservice');

class ApplicationAccessService {
  constructor() {
    this.dataService = new ApplicationAccessDataService()
  }

  createApplicationAccess(application, callback) {
    const applicationAccess = new ApplicationAccess(application.refex);

    return this.dataService.createApplicationAccess(applicationAccess, callback);
  }

  getApplicationAccessByAppRefex(app_refex, callback) {
    return this.dataService.getApplicationAccessByAppRefex(app_refex, callback);
  }
}

module.exports = ApplicationAccessService;