'use strict';

const AuthService = require('../v1/auth/auth.service');
const authService = new AuthService();

module.exports = {
  route: function(server) {
    server.get('/v1/auth/:type/:clientID', GETAcquiesce);
    //server.get('/v1/auth/:type/:clientID', GETAuthorize);
  }
}

function GETAcquiesce(req, res, next) {
  const options = {
    type    : req.params.type.toLowerCase(),
    clientID: req.params.clientID,
    hostname: req.headers.host
  }

  authService.validate(options)
    .then(options => {
      return authService.acquiesce(options);
    });
}

function GETAuthorize(req, res, next) {
  const options = {
    type    : req.params.type.toLowerCase(),
    clientID: req.params.clientID,
    hostname: req.headers.host
  }

  authService.validate(options)
    .then(options => {
      return authService.authorize(options);
    })
    .then(result => {
      res.send(result);
      next();
    });
}