'use strict';

const UUID = require('node-uuid');
const DomainEntity = require('icontactpoints-core').DomainEntity;

const schema = ['refex', 'app_refex'];

class ApplicationAccess extends DomainEntity {
  constructor(app_refex, params) {
    super();

    if (params) {
      this.refex = params.refex ? params.refex : UUID.v4().replace(/-/gi, '');     
    } else {
      this.refex = UUID.v4().replace(/-/gi, '');
    }

    this.app_refex = app_refex;
  }

  getAuthorizeCode() {
    return '';
  }
}

module.exports = ApplicationAccess;