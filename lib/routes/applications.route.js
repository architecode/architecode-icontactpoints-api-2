'use strict';

const ApplicationService = require('icontactpoints-core').ApplicationService;
const ApplicationAccessService = require('../v1/applicationaccesses/application.access.service');

const applicationService = new ApplicationService();
const applicationAccessService = new ApplicationAccessService();

module.exports = {
  route: function(server) {
    server.post('/v1/applications', POSTApplicationsHandler);
  }
}

function POSTApplicationsHandler(req, res, next) {
  const name = req.body.name;
  const contact_refex = req.body.contact_refex;
  const origins = req.body.origins;

  applicationService.createApplication(name, contact_refex, origins)
    .then(result => {
      if (result.success) {
        const application = result.data[0];
        const appResult = result;
        applicationAccessService.createApplicationAccess(application)
          .then(result => {
            if (result.success) {
              res.send(appResult);
            }
          })
      }
    })
    .catch(error => {
      res.send(error);
    });
}