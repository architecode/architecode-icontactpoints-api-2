const crypto = require('crypto');

// const basec = crypto.createDiffieHellman(128);

// const prime = basec.getPrime('hex');// 8b90e882a28df6381c90262eab21051b
// console.log(prime);

// const generator = basec.getGenerator('hex'); //02
// console.log(generator);

// const publ_key = basec.generateKeys('hex'); // 3fe0bff240259abd5e5205bf295d03f1
// console.log(publ_key);

// const priv_key = basec.getPrivateKey('hex'); // 7335b2fd8129aae92cb1da505e4e04d0
// console.log(priv_key);

// ===========================================================================

// Generate Alice's keys...
const alice = crypto.createDiffieHellman('8b90e882a28df6381c90262eab21051b', 'hex', '02', 'hex');
//const alice_key = alice.generateKeys('hex');
const alice_key = '3fe0bff240259abd5e5205bf295d03f1';

// Generate Bob's keys...
//const bob = crypto.createDiffieHellman('8b90e882a28df6381c90262eab21051b', 'hex', '02', 'hex');
const bob1 = crypto.createDiffieHellman(128);
//bob.setPublicKey('3fe0bff240259abd5e5205bf295d03f1', 'hex');
bob1.setPrivateKey('7335b2fd8129aae92cb1da505e4e04d0', 'hex');
//const bob_key = bob.generateKeys('hex');

// Exchange and generate the secret...
//const alice_secret = alice.computeSecret(bob_key, 'hex', 'hex');
const bob1_secret = bob1.computeSecret(alice_key, 'hex', 'hex');

// Generate Bob's keys...
//const bob = crypto.createDiffieHellman('8b90e882a28df6381c90262eab21051b', 'hex', '02', 'hex');
const bob2 = crypto.createDiffieHellman(128);
//bob.setPublicKey('3fe0bff240259abd5e5205bf295d03f1', 'hex');
bob2.setPrivateKey('7335b2fd8129aae92cb1da505e4e04d0', 'hex');
//const bob_key = bob.generateKeys('hex');

// Exchange and generate the secret...
//const alice_secret = alice.computeSecret(bob_key, 'hex', 'hex');
const bob2_secret = bob2.computeSecret(alice_key, 'hex', 'hex');

//console.log(alice_secret);
console.log(bob1_secret); //389a3ff5756ce9663a37337e6eddc1ca
console.log(bob2_secret); //389a3ff5756ce9663a37337e6eddc1ca