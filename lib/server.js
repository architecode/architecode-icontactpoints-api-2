'use strict';

const restify = require('restify');
const router = require('./router');

const server = restify.createServer({
  certificate: null,
  key: null,
  formatters: null,
  log: null,
  name: 'icontactpoints-api',
  spdy: null,
  version: '1.0.0',
  handleUpgrades: false
});

// sets middlewares
server.use(restify.queryParser());
server.use(restify.bodyParser({ mapParams: false }));

// sets the router
router.on(server);

// Run a server
server.listen(process.env.PORT || 8080, function() {
  console.log('%s listening at %s', server.name, server.url);
});