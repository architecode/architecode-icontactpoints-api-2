'use strict';

// Routes definition
const applicationsRoute = require('./routes/applications.route');
const authRoute         = require('./routes/auth.route');

const Router = {
  on: function(server) {
    applicationsRoute.route(server);
    authRoute.route(server);
  }
}

module.exports = Router;